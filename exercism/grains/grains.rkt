#lang racket

(provide square total)
;square n= combien de graines dans la case numéro n
;n= numéro de la case
(define (square n)
  (if (= n 1)
      1
      (* 2 (square (- n 1)))))

(define (total)
  (total2 64))

(define (total2 n)
  (if (= n 1)
      1
      (+ (total2 (- n 1))(square n))))
;64=total cases

